﻿using UnityEngine;
using System.Collections;

public class DuplicateObject : MonoBehaviour {

	public GameObject objectToBeDuplicated;

	public Vector2 numberOfObjects = Vector2.one * 4;

	public Vector2 offset = Vector2.one;

	public Transform targetTransform;

	public bool transformIsCenter;

	public Transform Parent;

	// Use this for initialization
	void Start () {
		//if no transform is provided
		if (targetTransform == null)
		{
			//use current transform
			targetTransform = this.transform;	
		}

		//if no transform is provided
		if (Parent == null)
		{
			//use current transform
			Parent = this.transform;	
		}
	
		Vector3 center;

		if (transformIsCenter)
			center = targetTransform.position / 2;
		else
			center = Vector3.zero;

		//loop for x
		for (int x = 0; x < numberOfObjects.x; x++) {

			//loop for y
			for (int y = 0; y < numberOfObjects.y; y++) {

				//create object
				GameObject newObject = Instantiate(objectToBeDuplicated, targetTransform.position + new Vector3(x*offset.x , y*offset.y, 0f) - center, targetTransform.rotation) as GameObject;

				newObject.transform.SetParent (Parent);
			}
		}

	}

}
