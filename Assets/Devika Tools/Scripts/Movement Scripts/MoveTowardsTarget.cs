﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Move/MoveTowardsTarget")] //this addes the compent to the Add Component Menu
public class MoveTowardsTarget : MonoBehaviour {

	//this component needs a MoveSpeed Component & Target Component

	//How close the target needs to be before it starts to follow
	[Tooltip("How close the target needs to be before it is followed")]
	public float maximumRange;

	//How close to the target it will get
	[Tooltip("How close it will get to the target before stopping")]
	public float minimumRange;

	//If we use velocity or just manual shift the position
	public bool withPhsyics = false;

	// Update is called once per frame
	void Update () {

		//if we are using phsyics
		if (withPhsyics) {

			//stop moving 
			GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		}

		//If the target is close enough
		if (Vector3.Distance (this.transform.position, GetComponent<Target>().target.position) <= maximumRange) 
		{
			//if the target is not too close
			if (Vector3.Distance (this.transform.position, GetComponent<Target>().target.position) >= minimumRange) 
			{
				//if we are using phsyics
				if (withPhsyics) {

					//Rotate to face target
					/*float x = transform.rotation.eulerAngles.x;
					float y = transform.rotation.eulerAngles.y;

					Debug.Log ("x" + transform.rotation.eulerAngles.x + " z" + transform.rotation.eulerAngles.z);
					transform.LookAt( GetComponent<Target>().target);
					transform.rotation = Quaternion.Euler (new Vector3 (x,y,transform.rotation.eulerAngles.z));
					Debug.Log ("x" + transform.rotation.eulerAngles.x+ " z" + transform.rotation.eulerAngles.z);*/

					GetComponent<Rigidbody2D> ().velocity = (GetComponent<Target>().target.position- transform.position).normalized * GetComponent<MoveSpeed> ().speed;
						
				} else {

					//This adjusts the speed based on the framefrate (also know as clockspeed) of the computer
					float step = GetComponent<MoveSpeed>().speed * Time.deltaTime;//deltaTime is the difference in time between each frame. You could also desribe it as how long each frame takes.

					//Move towards the target
					this.transform.position = Vector3.MoveTowards (this.transform.position, GetComponent<Target> ().target.position, step);
				}
			}
		}
	}
}
