﻿//by Brennan Hatton - May 2016 brennan@brennanhatton.com

using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Move/MoveDirection")] //this addes the compent to the Add Component Menu
public class MoveDirection : MonoBehaviour {

	//Thi holds the direction and speed you want object to move
	public Vector3 Direction;

	Rigidbody2D rigidbody;

	void Start()
	{
		//Get the rigidbody component
		rigidbody = this.GetComponent<Rigidbody2D> ();

		//If there is no rigibody componet
		if (rigidbody == null) {

			Debug.LogWarning ("PlayerMovePlatformer component requires Ridigbody on " + this.gameObject.name + ". One was added");

			//add the rigidbody component
			rigidbody = this.gameObject.AddComponent<Rigidbody2D> ();
		}
	}

	// Update is called once per frame
	void Update () {
	
		//Add the movement Direction to the curret position. Moving it in that direction
		rigidbody.velocity = Direction.normalized * GetComponent<MoveSpeed>().speed;

	}
}
