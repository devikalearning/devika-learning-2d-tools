﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Move/MoveTurnFromWalls")] //this addes the compent to the Add Component Menu
public class MoveTurnFromWalls : MonoBehaviour {

	//Speed object moves
	public float speed = 3;

	//has it turned around?
	bool forward = true;


	// Update is called once per frame
	void Update () {

		//A raycast is like pointing out a lazer from the provided position, in the provided direction for the provided distance and seeing it if hits anything.
		RaycastHit2D hit;

		//If we are moving forward
		if (forward == true)
		{
			//Keep moving forward
			this.GetComponent<Rigidbody2D> ().velocity = Vector2.right*speed;


			//Check in front if there is a wall
			hit = Physics2D.Raycast(new Vector2(transform.position.x,transform.position.y) , Vector2.right, this.GetComponent<BoxCollider2D> ().size.x*Mathf.Abs(transform.localScale.x)/2+0.1f, (LayerMask)1);
		}
		else
		{
			//move backward
			this.GetComponent<Rigidbody2D> ().velocity = Vector2.left*speed;

			//Check ahead if there is a wal
			hit = Physics2D.Raycast(new Vector2(transform.position.x,transform.position.y) , Vector2.left, this.GetComponent<BoxCollider2D> ().size.x*Mathf.Abs(transform.localScale.x)/2+0.1f, (LayerMask)1);
		}

		//If we hit a wall
		if (hit.collider != null)
		{
			//Turn the object around
			if (forward == true)
				forward = false;
			else
				forward = true;

			//if the player has a sprite
			if (GetComponent<SpriteRenderer> ()) {
				//flip image
				GetComponent<SpriteRenderer> ().flipX = !forward;
			}
		}



	}
}
