﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Move/MoveAlongPointPath")] //this addes the compent to the Add Component Menu
public class MoveAlongPointPath : MonoBehaviour {

	//List of points that make up path
	//By having [] after the variable type, it makes it an Array. Which is like a list of that type. (The word list means something else, but similar in programming)
	//Here we have an Array of transforms. Meaning there is mutliple. You will notice in the inspector you can pick how many
	public Transform[] pathPoints;

	//Will the path loop or just trace back
	public bool loop = true;

	//Is it currently follwing the path forward or backward
	bool forward;

	//Do we start the loop at the cloests point, or at the first point
	public bool StartAtClosestPoint = true;

	//The next point to move towards
	int nextPoint = 0;

	//A reference to the rigidbody2D component, so we dont have ot keep using GetComponent each frame because it can be slow
	Rigidbody2D rigidBody;

	// Use this for initialization
	void Start () {

		//Get a reference to the Rigidbody component
		rigidBody = this.GetComponent<Rigidbody2D>();


		//if we re starting at closest point
		if (StartAtClosestPoint) {


			//find closest point
			//if we found some
			if (pathPoints.Length > 0) {
				//Save first one
				nextPoint = 0;

				//Calculate distance to first one
				float nextDistance, closestDistance = Vector3.Distance (this.transform.position, pathPoints [nextPoint].position);

				//For all other objects
				for (int i = 1; i < pathPoints.Length; i++) {
				
					//calculate distance to object
					nextDistance = Vector3.Distance (this.transform.position, pathPoints [i].position);

					//if this object is closer
					if (nextDistance < closestDistance) {
						//update closest distance
						closestDistance = nextDistance;

						//save obejct as closest so far
						nextPoint = i;
					}
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
		//if not points
		if (pathPoints.Length <= 1) {

			//exit
			return;
		}

		//check if at next point
		if(Vector3.Distance(pathPoints[nextPoint].position, this.transform.position) < GetComponent<MoveSpeed>().speed*Time.deltaTime)
		{
			//if we are looping
			if (loop) 
			{
				//go to next point
				//% is called mod, or modula. It is like a devide, but it only returns the remainder.
				//We can use it to do a loop. 
				// if we have : a = b % c. 
				//'a' can never be higher than 'c'. 
				nextPoint = (nextPoint + 1) % pathPoints.Length;
			}
			else //we are not looping
			{
				//if we are moving forward
				if (forward) {
					//if the next point is before the end
					if (nextPoint < pathPoints.Length - 1) {
						//move to next point
						nextPoint++;

					} else { //the next point is at the end
						//start going backwards
						forward = !forward;
					}
				} else { //we are moving backwards
					//if the next point is not at the begining
					if (nextPoint > 0) {
						//move backwards
						nextPoint--;
					} else//the next point is at the beginning
					{
						//startgoing forwards
						forward = !forward;
					}
				}
			}
		}

		//This adjusts the speed based on the framefrate (also know as clockspeed) of the computer
		float step = GetComponent<MoveSpeed>().speed * Time.deltaTime;//deltaTime is the difference in time between each frame. You could also desribe it as how long each frame takes.

		//calculate angle to nxt point
		var dir = pathPoints[nextPoint].position - transform.position;
		var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

		//Apply force in right direction
		rigidBody.velocity = transform.right * GetComponent<MoveSpeed>().speed;


	}
}
