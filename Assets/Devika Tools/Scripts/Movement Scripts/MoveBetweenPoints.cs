﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Move/MoveBetweenPoints")] //this addes the compent to the Add Component Menu
public class MoveBetweenPoints : MonoBehaviour {

	//The two objects it is moving towards
	public Transform transform1, transform2;

	//The directoin it is mvoing
	public bool forward;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		//This adjusts the speed based on the framefrate (also know as clockspeed) of the computer
		float step = GetComponent<MoveSpeed>().speed * Time.deltaTime;//deltaTime is the difference in time between each frame. You could also desribe it as how long each frame takes.

		//if we are moving forward
		if (forward) {
			//move from current transform towards transform1 by the amount of step
			transform.position = Vector3.MoveTowards (this.transform.position, transform1.position, step);
		} else {
			//move from current transform towards transform2 by the amount of step
			transform.position = Vector3.MoveTowards (this.transform.position, transform2.position, step);
		}
	}
}
