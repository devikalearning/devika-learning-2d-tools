﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Move/MoveTowardsObject")] //this addes the compent to the Add Component Menu
public class MoveTowardsObject : MonoBehaviour {

	public Transform targetObject;

	//How close the target needs to be before it starts to follow
	[Tooltip("How close the target needs to be before it is followed")]
	public float maximumRange;

	//How close to the target it will get
	[Tooltip("How close it will get to the target before stopping")]
	public float minimumRange;


	// Update is called once per frame
	void Update () {

		//This adjusts the speed based on the framefrate (also know as clockspeed) of the computer
		float step = GetComponent<MoveSpeed>().speed * Time.deltaTime;//deltaTime is the difference in time between each frame. You could also desribe it as how long each frame takes.

		//If the target is close enough
		if (Vector3.Distance (this.transform.position, targetObject.position) <= maximumRange) 
		{
			//if the target is not too close
			if (Vector3.Distance (this.transform.position, targetObject.position) >= minimumRange) 
			{
				//Move towards the target
				this.transform.position = Vector3.MoveTowards (this.transform.position, targetObject.position, step);
			}
		}
	}
}
