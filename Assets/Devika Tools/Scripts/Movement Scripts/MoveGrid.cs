﻿using UnityEngine;
using System.Collections;

public class MoveGrid : MonoBehaviour {

	//
	public Vector3 gridSize = Vector3.one; 

	public Vector3 direction;

	Vector3 position;

	// Use this for initialization
	void Start () {
		position = this.transform.position;
	}

	// Update is called once per frame
	void Update () {

		float step = GetComponent<MoveSpeed>().speed * Time.deltaTime;
		position = Vector3.MoveTowards (position, position + direction.normalized * GetComponent<MoveSpeed>().speed, step);

		transform.position = new Vector3 ((float)(int)(position.x / gridSize.x), (float)(int)(position.y / gridSize.y), (float)(int)(position.z / gridSize.z));
	}
}
