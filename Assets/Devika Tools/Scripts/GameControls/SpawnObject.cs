﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/GameControls/SpawnObject")] //this addes the compent to the Add Component Menu
public class SpawnObject : MonoBehaviour {

	//This si the objct we will be spawning
	public GameObject objectToSpawn;

	//How often it is spawned
	public float frequency = 10f;

	//Distance from position they are to be spawned
	public float minDistance = 10f;
	public float maxDistance = 15f;

	//position objects are spawned from
	public Transform position;


	//how long until next spawn
	float timer;

	// Use this for initialization
	void Start () {

		//reset timer
		timer = frequency;

		//if not posotion is provided use self
		if (position == null) {
			position = this.transform;
		}

	}
	
	// Update is called once per frame
	void Update () {

		//if its time for next object
		if (timer <= 0) {

			//Calculate position on circle
			float angle = Random.value * Mathf.PI * 2;
			//float radius = _random.NextDouble() * _radius;
			float distance = minDistance + (maxDistance - minDistance) * Random.value;
			float x = position.position.x + minDistance * Mathf.Cos(angle);
			float y = position.position.y + minDistance * Mathf.Sin(angle);

			//Create object
			Instantiate (objectToSpawn, new Vector3(x,y,0f), objectToSpawn.transform.rotation);

			//reset timer
			timer = frequency;

		}else
		{
			//update timer
			timer -= Time.deltaTime;
		}
	}
}
