﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.IO;

[AddComponentMenu("Devika/GameControls/GameController")] //this addes the compent to the Add Component Menu
public class GameController : MonoBehaviour {

	//A statc variable can be called without have a reference ot any object. 
	//A public variable can be accessed from outside of the class GameController. That is why we can see them in the inspector
	//This variable is both public and staic
	//This means, any script can access the variable instance.
	//It is a reference to this object. So all scripts can talk to this object
	public static GameController instance;

	//The game score
	[HideInInspector]
	public int score;

	//Te key to quit the game
	public KeyCode QuitKey = KeyCode.Escape;

	//the key to pause the game (not yet working)
	///public KeyCode PauseKey = KeyCode.Return;

	//UI References that are set in the inspector
	public GameObject GameUI;
	public GameObject MenuUI;
	public Text scoreText;
	public Text highScoreText;
	GameObject ScoreUI;

	//The highest score
	[HideInInspector]
	public int highScore = 0;

	//the next level the game will move onto
	int nextLevel;

	//save file name
	string fileName = "savefile.txt";

	//First thing this object does
	void Awake() {

		//if there is already a GameController
		if (instance != null)
		{
			//Destory this object, we dont need two
			DestroyImmediate(this.gameObject);

			//exit if hasnt already
			return;
		}

		//Make this the one and only Game Controller
		instance = this;

		//dont destory this object between scenes
		DontDestroyOnLoad(this.gameObject);

		//Load high score
		LoadHighScore();
	}

	//This is called at the start of a new scene
	void Start()
	{
		//Call function for new level
		OnLevelWasLoaded (Application.loadedLevel);
	}

	/// Raises the level was loaded event.
	void OnLevelWasLoaded(int level)
	{
		//Increase nextLevel number
		nextLevel++;

		//Check if Menu
		if (level == 0) {

			//menu UI is turned on
			MenuUI.gameObject.SetActive (true);

			//Game UI is turned off
			GameUI.gameObject.SetActive (false);

		} else //a game level
		{
			//if first level
			if (level == 1) {

				//Reset score
				score = 0;
			}

			//Turn off MenuUI
			MenuUI.gameObject.SetActive (false);

			//Turn on game UI
			GameUI.gameObject.SetActive (true);


		}
	}

	// Update is called once per frame
	void Update () {

		//Update score display
		scoreText.text = "Score: " + score.ToString ();

		//If we pressed the quit button
		if (Input.GetKeyDown (QuitKey)) 
		{

			//Is the the main menu?
			if (Application.loadedLevel == 0) 
			{
				//Exit the game
				Application.Quit ();
			} else // it is not the main menu
			{ 
				//End game to main menu
				EndGame ();
			}
		}

	}

	//Start the game
	public void PlayGame()
	{
		//load the first level
		Application.LoadLevel(1);
	}

	//Go to next level
	public void NextLevel()
	{
		
		//Check if at end
		if (Application.levelCount - 1 <= nextLevel) 
		{
			//End the game ot main menu
			EndGame ();
		}

		//load next level
		Application.LoadLevel(nextLevel);
	}

	//Ends the game and goes to main menu
	public void EndGame()
	{
		//rset nextLevel
		nextLevel = 0;

		//Turn on MenuUI
		MenuUI.gameObject.SetActive (true);

		//Turn off GameUI
		GameUI.gameObject.SetActive(false);

		//If we broke the highscore
		if (score > highScore) 
		{
			//Set new hgihscore!
			highScore = score;

			//save to highscrore file!
			SaveHighScore ();
		}

		//update highscore text
		highScoreText.text = "High Score: " + highScore.ToString ();

		//load main menu
		Application.LoadLevel("MainMenu");
	}

	void LoadHighScore()
	{
		//This code currently doesnt work on all computers. Remove * to test on yours.

		/*/If highscore file exists
		if (File.Exists(fileName))
		{
			//Read file into memory
			StreamReader  sr = File.OpenText(fileName);
			//Covert to text
			string line = sr.ReadLine();
			//Conver to Int and save
			highScore = Convert.ToInt32 (line);
			sr.Close();

			highScoreText.text =  "High Score: " + highScore.ToString ();

		}
		else 
		{
			StreamWriter sw = File.CreateText(fileName);
			sw.Close();
		}//*/
	}

	void SaveHighScore()
	{
		//This code currently doesnt work on all computers. Remove * to test on yours.

		/*/If highscore file exists
		if (File.Exists(fileName))
		{
			File.Delete (fileName);

		}

		StreamWriter sw = File.CreateText(fileName);
		sw.WriteLine (highScore.ToString ());
		sw.Close();
		//*/
	}
}
