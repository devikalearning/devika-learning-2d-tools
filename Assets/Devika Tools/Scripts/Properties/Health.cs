﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; //We tells the game we want to talk to the UI (User Interface). This way we can talk to the Slider UI.

[AddComponentMenu("Devika/Properties/Health")] //this addes the compent to the Add Component Menu
public class Health : MonoBehaviour {

	//health of object
	public float health = 100;

	//reference to health bar visual
	public Slider healthBar;

	//If the death of this object ends the game (usually used on player)
	public bool deadEndsGame;

	//The starting health of the object
	float startHealth;

	void Start()
	{
		//set starting health
		startHealth = health;
	}

	void Update(){

		//if the objct has run out of health
		if (IsAlive () == false) 
		{
			//check if killing this player ends the game
			if (deadEndsGame) {
				//END THE GAME!
				GameController.instance.EndGame ();
			} else {
				DestroyObject (this.gameObject);
			}
		}

		
		//If we are using a healthBar
		if (healthBar != null) {
			healthBar.value = health / startHealth * 100;
		}
	}

	public bool IsAlive()
	{
		//If the player has health
		if (health > 0)
			//it is alive!
			return true;

		//Once a return has been called, the function is ended
		//This will only be reached if the "return true;" above was not
		return false;
	}
}
