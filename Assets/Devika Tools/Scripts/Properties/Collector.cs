﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Properties/Collector")] //this addes the compent to the Add Component Menu
public class Collector : MonoBehaviour {

	[HideInInspector]
	public bool hasHealth;

	void Start()
	{
		//If this object has a health component
		if (GetComponent<Health> () != null)
			//It has health
			hasHealth = true;
		else
			//it does not have health
			hasHealth = false;
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		Collectable collectable = other.GetComponent<Collectable> ();

		if (collectable)
			collectable.Collect (this);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		Collectable collectable = col.gameObject.GetComponent<Collectable> ();

		if (collectable)
			collectable.Collect (this);
	}
}
