﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Properties/RandomizeAmmo")] //this addes the compent to the Add Component Menu
public class RandomizeAmmo : MonoBehaviour {

	//Maximum ammo
	public int maxAmmo;

	//Minimum ammo
	public int minAmmo;

	void Start()
	{
		//Rnndomly set ammo between minimum and max
		GetComponent<Ammo> ().ammo = Random.Range (minAmmo, maxAmmo);
	}
}
