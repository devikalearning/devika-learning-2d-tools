﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Properties/MoveSpeed")] //this addes the compent to the Add Component Menu
public class MoveSpeed : MonoBehaviour {

	//The speed the object moves
	public float speed = 1;

}
