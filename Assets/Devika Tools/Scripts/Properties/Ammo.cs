﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Properties/Ammo")] //this addes the compent to the Add Component Menu
public class Ammo : MonoBehaviour {

	//ammount of ammo left
	public int ammo;

	//can we keep using ammo if we are at zero
	public bool stopAtZero;

	public bool AmmoLeft()
	{
		//if we stop at zero
		if (stopAtZero) {

			//if we have less than or equal to zero
			if (ammo <= 0) {

				//there is no ammo left
				return false;
			}
				
		}

		//there is ammo left!
		return true;
	}


}
