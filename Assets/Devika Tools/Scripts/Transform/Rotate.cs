﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Transform/Rotate")] //this addes the compent to the Add Component Menu
public class Rotate : MonoBehaviour {

	/// The rotation speed.
	public Vector3 RotationSpeed;

	void Update()
	{
		//Rotate the object by the rotation speed
		this.transform.Rotate (RotationSpeed);
	}
}
