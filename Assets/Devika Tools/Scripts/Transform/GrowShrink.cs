﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Transform/GrowShrink")] //this addes the compent to the Add Component Menu
public class GrowShrink : MonoBehaviour {

	//Maximum size reached
	public Vector3 maxSize = Vector3.one * 2;

	//Minimum size reached
	public Vector3 minSize = Vector3.one;

	//Speed it changes size
	public float speed = 1;

	//is it currently growing or shrinking
	bool growing = true;

	//timer object was created
	float startTime;

	//how far it changes
	float growthChange;

	void Start()
	{
		//save starting time
		startTime = Time.time;

		//Caculate distance object will grow
		growthChange = Vector3.Distance(maxSize, minSize);

		//Start at minimum size
		this.transform.localScale = minSize;
	}
	
	// Update is called once per frame
	void Update () {

		//how far has it grown
		float distCovered = (Time.time - startTime) * speed;

		//fraction of growth covered
		float fracJourney = distCovered / growthChange;

		//if it is growing
		if (growing) {
			//if it is smaller than the maximum size
			if (this.transform.localScale.x < maxSize.x) {
				//Keep growing!
				this.transform.localScale = Vector3.Lerp (minSize, maxSize, fracJourney);
			} else { //it is larger tha the maximum size
				//Restart the clock
				startTime = Time.time;

				//Now it is shrinking
				growing = false;
			}
		} else { ///it is shrinking
			//if it is larger than the mimimum size
			if (this.transform.localScale.x > minSize.x)
			{
				//shrink!
				this.transform.localScale = Vector3.Lerp (maxSize, minSize, fracJourney);
			}
			else //it is smaller than the mimimum
			{
				//restart clock
				startTime = Time.time;

				//now it is growing
				growing = true;
			}
		}
	
	}
}
