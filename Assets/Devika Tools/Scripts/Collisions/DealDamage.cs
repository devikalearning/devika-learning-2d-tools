﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnCollision/DealDamage")] //this addes the compent to the Add Component Menu
public class DealDamage : MonoBehaviour {

	//the amount of damage done to health
	public float damage;

	void OnTriggerEnter2D(Collider2D other)
	{
		//Get the health component of other object
		Health health = other.GetComponent<Health> ();

		//If it has a health component
		if (health) {

			//take away damage from health
			health.health -= damage;
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		//Get the health component of other object
		Health health = col.gameObject.GetComponent<Health> ();

		//If it has a health component
		if (health) {

			//take away damage from health
			health.health -= damage;
		}
	}
}
