﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnCollision/CreateOnTouch")] //this addes the compent to the Add Component Menu
public class CreateOnTouch : MonoBehaviour {


	//Object to create
	public GameObject ObjectToCreate;

	//Position & Rotation to create object at
	[Tooltip("Leave blank for current transform")]//Tool tips show up in the inspector
	public Transform targetTransform;

	//Tag of object needed to touch
	[Tooltip("Leave blank for no specific tag, will work with all objects")]//Tool tips show up in the inspector
	public string tag;

	//Will not create from touching triggers
	public bool notTriggers = false;

	//Is it created ina  relative positino to this object
	public bool relative = false;

	void Start()
	{
		//if no transform is provided
		if (targetTransform == null)
		{
			//use current transform
			targetTransform = this.transform;	
		}

		//if it is created in a  relative positino to this object
		if (relative) {

			//add the positions
			targetTransform.position += transform.position;

			//rotate by rotation
			targetTransform.Rotate(transform.eulerAngles);
		}
	}

	//When this object hits a trigger
	void OnTriggerEnter2D(Collider2D other)
	{
		//if we are not creating on triggers
		if (notTriggers) {
			//exit this function, we dont want to contiune to creating the game object
			return;
		}

		//if tag is empty or correct
		if (tag == null || tag == "" || other.tag == tag) 
		{
			//create object at specified transform
			Instantiate(ObjectToCreate, targetTransform.position, targetTransform.rotation);
		}
	}

	//When ths object has a collision
	void OnCollisionEnter2D(Collision2D col)
	{

		//if tag is empty or correct
		if (tag == null || tag == ""  || col.gameObject.tag == tag)
		{
			//create object at specified transform
			Instantiate(ObjectToCreate, targetTransform.position, targetTransform.rotation);
		}
	}
}
