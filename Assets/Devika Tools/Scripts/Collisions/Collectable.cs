﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnCollision/Collectable")] //this addes the compent to the Add Component Menu
public class Collectable : MonoBehaviour {

	public int score;
	public int health;
	public bool win;

	public void Collect(Collector collector)
	{
		//Add the score
		GameController.instance.score += score;

		//Check if the object collecting has health
		if (collector.hasHealth)
			//Add health
			collector.GetComponent<Health>().health += health;

		//Check if collecting this object should win the gmae
		if (win) {
			//Game over!!
			GameController.instance.NextLevel ();
		}

		//Destroy the collectable 
		Destroy (this.gameObject);
	}
}
