﻿//by Brennan Hatton - May 2016 brennan@brennanhatton.com

using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnCollision/Teleporter")] //this addes the compent to the Add Component Menu
public class Teleporter : MonoBehaviour {

	//Location objects end up after teleporting
	public Transform TeleportEndLocation;

	//Switches to either follow the transform of the target or stick to original transform
	public bool LockY = true;
	public bool LockZ = true;


	void OnTriggerEnter2D(Collider2D other)
	{
		Teleport (other.transform);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		Teleport (col.gameObject.transform);
	}


	/// Teleport the specified teleporteeTransform.
	public void Teleport(Transform teleporteeTransform)
	{
		//Save the new position
		Vector3 newPosition = TeleportEndLocation.transform.position;

		//Change so the y and z are the same as the teleportee
		if (LockY)
			newPosition.y = teleporteeTransform.position.y;
		if (LockZ)
			newPosition.z = teleporteeTransform.position.z;

		//teleport the teleportee
		teleporteeTransform.position = newPosition;

	}
}
