﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnCollision/DestroyOnTouch")] //this addes the compent to the Add Component Menu
public class DestroyOnTouch : MonoBehaviour {

	//the tag we need to touch us
	[Tooltip("Leave blank for everything")]
	public string tag;

	//Do we include triggers
	public bool notTriggers = false;

	//When this object hits a trigger
	void OnTriggerEnter2D(Collider2D other)
	{
		//if we are not desotrying on triggers
		if (notTriggers) {
			//exit this function, we dont want to contiune to destory this game object
			return;
		}

		//if the tag is the same or blank
		if (tag == null || tag == ""  || other.tag == tag)
			//Destory this game object (if we just say "DestroyObject (this)" it will only destory this component, not the whole game object)
			DestroyObject (this.gameObject);
	}

	//When ths object has a collision
	void OnCollisionEnter2D(Collision2D col)
	{
		//if the tag is the same or blank
		if (tag == null || tag == "" || col.gameObject.tag == tag)
			
			//desotry this game object
			DestroyObject (this.gameObject);
	}
}
