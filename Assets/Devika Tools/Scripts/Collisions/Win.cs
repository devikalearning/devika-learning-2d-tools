﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnCollision/Win")] //this addes the compent to the Add Component Menu
public class Win : MonoBehaviour {

	public int targetScore;

	public Collectable[] requiredCollectables;

	void OnTriggerEnter2D(Collider2D other)
	{
		for (int i = 0; i < requiredCollectables.Length; i++) {
			if (requiredCollectables[i] != null)
				return;
		}

		if (GameController.instance.score >= targetScore && other.GetComponent<Collector> ()) {

			//Game over!!
			GameController.instance.NextLevel ();
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		for (int i = 0; i < requiredCollectables.Length; i++) {
			if (requiredCollectables[i] != null)
				return;
		}

		if (GameController.instance.score >= targetScore && col.gameObject.GetComponent<Collector> ()) {

			//Game over!!
			GameController.instance.NextLevel ();
		}
	}
}
