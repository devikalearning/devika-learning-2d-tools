﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Player/PlayerSprint")] //this addes the compent to the Add Component Menu
public class PlayerSprint : MonoBehaviour {

	//sprint key
	public KeyCode sprintKey = KeyCode.LeftShift;

	//how much to increase speed by when spriting.
	public float sprintSpeed = 1f;

	float timer = 0;

	//regular speed so we can change it back
	float regularSpeed;

	// Update is called once per frame
	void Update () {

		//If we just pressed down the sprint key
		if (Input.GetKeyDown (sprintKey)) 
		{
			//if we havent been sprinting too long
			//if (timer > 0) 
			//{

				//Save the old speed, so we can return it to walking after sprinting
				regularSpeed = GetComponent<MoveSpeed> ().speed;

				//Set speed to sprint speed
				GetComponent<MoveSpeed> ().speed = sprintSpeed;

				//decrease timer
			//	timer -= Time.deltaTime;
			//}
		}

		//if we just released the spint key
		if (Input.GetKeyUp (sprintKey)) 
		{
			//Set the move speed to be the original walking speed
			GetComponent<MoveSpeed> ().speed = regularSpeed;

			//reset timer
			//timer = sprintTime;
		}
	}
}
