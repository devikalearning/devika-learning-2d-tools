﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Player/PlayerMoveDirectionFace")] //this addes the compent to the Add Component Menu
public class PlayerMoveDirectionFace : MonoBehaviour {

	//Key controls
	//"public" means it can seen and changed in the Unity Inspector Window.
	//"KeyCode" means it can have values as of the keys on your keyboard. like A, B, C, Return, Space, Escape
	//"upKey" is the name of the variable
	public KeyCode upKey = KeyCode.W;
	public KeyCode downKey = KeyCode.S;
	public KeyCode leftKey = KeyCode.A;
	public KeyCode rightKey = KeyCode.D;

	//if player keeps moving after keypress
	public bool continuousMovement;

	//if there is drag
	public bool drag;

	//A reference to the physics componet
	Rigidbody2D rigidbody;

	// Use this for initialization
	void Start () {

		//Get the rigidbody component
		rigidbody = this.GetComponent<Rigidbody2D> (); //Calling GetComponent every frame can make the game run slow. 
		//Here we save the result from the GetComponent into a variable, so we dont have to call it in the Update loop

		//If there is no rigibody componet
		if (rigidbody == null) {

			Debug.LogWarning ("PlayerMovePlatformer component requires Rigidbody2D on " + this.gameObject.name + ". One was added");

			//add the rigidbody component
			rigidbody = this.gameObject.AddComponent<Rigidbody2D> ();
		}
	}

	// Update is called once per frame
	void Update () {

		//if we are not contiuniously moving
		if (continuousMovement == false) {
			//stop moving the player
			rigidbody.velocity = Vector2.zero;
		}

		//reset xVelocity
		float xVelocity = 0;
		//reset yVelocity
		float yVelocity = 0;

		//if drag
		if (drag)
		{
			//Keep the same X Velocity
			xVelocity = rigidbody.velocity.x;

			//Keep the same X Velocity
			yVelocity = rigidbody.velocity.y;
		}


		if (Input.GetKey(upKey))
		{
			//Update velocity with speed and xVelocity
			Vector2 VectorBeforeRotate = new Vector2(xVelocity, GetComponent<MoveSpeed>().speed);

			float sin = Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad);
			float cos = Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad);

			float tx = VectorBeforeRotate.x;
			float ty = VectorBeforeRotate.y;
			float newX = (cos * tx) - (sin * ty);
			float newY = (sin * tx) + (cos * ty);

			rigidbody.velocity = new Vector2 (newX, newY);


		}

		if (Input.GetKey(downKey))
		{
			//Update velocity with speed and xVelocity
			Vector2 VectorBeforeRotate = new Vector2(xVelocity, -GetComponent<MoveSpeed>().speed);

			float sin = Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad);
			float cos = Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad);

			float tx = VectorBeforeRotate.x;
			float ty = VectorBeforeRotate.y;
			float newX = (cos * tx) - (sin * ty);
			float newY = (sin * tx) + (cos * ty);

			rigidbody.velocity = new Vector2 (newX, newY);
		}

		if (Input.GetKey(leftKey))
		{

			//Update velocity with speed and xVelocity
			Vector2 VectorBeforeRotate = new Vector2 (-GetComponent<MoveSpeed> ().speed, yVelocity);

			float sin = Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad);
			float cos = Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad);

			float tx = VectorBeforeRotate.x;
			float ty = VectorBeforeRotate.y;
			float newX = (cos * tx) - (sin * ty);
			float newY = (sin * tx) + (cos * ty);

			rigidbody.velocity = new Vector2 (newX, newY);

		}

		if (Input.GetKey(rightKey))
		{

			//Update velocity with speed and xVelocity
			Vector2 VectorBeforeRotate = new Vector2 (GetComponent<MoveSpeed> ().speed, yVelocity);

			float sin = Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad);
			float cos = Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad);

			float tx = VectorBeforeRotate.x;
			float ty = VectorBeforeRotate.y;
			float newX = (cos * tx) - (sin * ty);
			float newY = (sin * tx) + (cos * ty);

			rigidbody.velocity = new Vector2 (newX, newY);

		}
	}
}
