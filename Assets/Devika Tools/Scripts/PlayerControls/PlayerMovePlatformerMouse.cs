﻿using UnityEngine;
using System.Collections;

public class PlayerMovePlatformerMouse : MonoBehaviour {

	//Jump force
	public float jumpForce = 10f;

	//A reference to the physics componet
	Rigidbody2D rigidbody;

	// Use this for initialization
	void Start () {

		//Get the rigidbody component
		rigidbody = this.GetComponent<Rigidbody2D> ();

		//If there is no rigibody componet
		if (rigidbody == null) {

			Debug.LogWarning ("PlayerMovePlatformer component requires Rigidbody2D on " + this.gameObject.name + ". One was added");

			//add the rigidbody component
			rigidbody = this.gameObject.AddComponent<Rigidbody2D> ();
		}
	}
	
	// Update is called once per frame
	void Update () {

		//Stop moving the player sideways, but keep vertical velocity
		GetComponent<Rigidbody2D>().velocity = new Vector2(0,GetComponent<Rigidbody2D>().velocity.y);

		//If the mouse is pressed
		if (Input.GetMouseButton(0))
		{
			//Move right, but keep vertical velocity
			if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x < this.transform.position.x)
			{
				rigidbody.velocity = new Vector2(-GetComponent<MoveSpeed>().speed,rigidbody.velocity.y);

				//if the player has a sprite
				if (GetComponent<SpriteRenderer> ()) {
					//flip image
					GetComponent<SpriteRenderer> ().flipX = true;
				}
			}

			//Move right, but keep vertical velocity
			else if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x > this.transform.position.x)
			{
				rigidbody.velocity = new Vector2(GetComponent<MoveSpeed>().speed,rigidbody.velocity.y);

				//if the player has a sprite
				if (GetComponent<SpriteRenderer> ()) {
					//flip image
					GetComponent<SpriteRenderer> ().flipX = false;
				}
			}
				
		}

		if (Input.GetMouseButtonDown(0)) {

			//Calculating jump

			//use trigonometry to calculate the angle the mouse is away from vertical.
			/*		Y
			 * 		|			<-
			 * 		|			 \	|
			 * 		|		      \-|
			 * 		|			   \|
			 * 		|				O
			 * 		+--------------------------------X
			 * 	
			 * 
			 *  O = player
			 *  <- = mouse
			 *  - = angle
			 */


			//We need to find the angle.

			//Sin(angle) = Opposite / Hypotenuse
			//but .. Rearranged
			//angle = AarcSin( Opposite  / Hypotenuse );

			float opposite = this.transform.position.x - Camera.main.ScreenToWorldPoint (Input.mousePosition).x;

			//Pythagoras
			//h^2 = a ^ 2 + b ^ 2
			//h = sqrt(a ^ 2 + b ^ 2);
			float adjacent = this.transform.position.y - Camera.main.ScreenToWorldPoint (Input.mousePosition).y;
			float hypotenuse = Mathf.Sqrt (opposite * opposite + adjacent * adjacent);

			//Back to our formula for the angle
			//angle = AarcSin( Opposite  / Hypotenuse );
			float angle = Mathf.Asin (opposite / hypotenuse);

			//If the mouse is above
			//Angle is in Radians 
			if (Mathf.Abs (angle) < 1) {
				//A raycast is like pointing out a lazer from the provided position (transform.position), in the provided direction (-Vector3.up) for the provided distance (0.5f) and seeing it if hits anything.
				RaycastHit2D hit = Physics2D.Raycast (new Vector2 (transform.position.x, transform.position.y), -Vector2.up, 1f, (LayerMask)1);

				// If on the ground and jump is pressed...
				if (hit.collider != null)
					// ... add force in upwards.
					rigidbody.AddForce (Vector2.up * jumpForce, ForceMode2D.Impulse);
			}
		}
		
	}
}
