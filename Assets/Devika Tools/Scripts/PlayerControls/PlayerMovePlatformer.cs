using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Player/PlayerMovePlatformer")] //this addes the compent to the Add Component Menu
public class PlayerMovePlatformer : MonoBehaviour {

	//Below are variables. Variables are names that hold numbers which can change value.
 
	//variables for which keys to move the player
	//"public" means it can seen and changed in the Unity Inspector Window.
	//"KeyCode" means it can have values as of the keys on your keyboard. like A, B, C, Return, Space, Escape
	//"leftKey" is the name of the variable
	public KeyCode leftKey = KeyCode.A;
	public KeyCode rightKey = KeyCode.D;


	// Update is called once per frame
	void Update () {

		//Stop moving the player sideways, but keep vertical velocity
		GetComponent<Rigidbody2D>().velocity = new Vector2(0,GetComponent<Rigidbody2D>().velocity.y);

		//Check if pressing left mouse button
		if (Input.GetKey(leftKey))
		{
			//move left, but keep vertical velocity
			GetComponent<Rigidbody2D>().velocity = new Vector2(-GetComponent<MoveSpeed>().speed,GetComponent<Rigidbody2D>().velocity.y);

			//flip image
			GetComponent<SpriteRenderer>().flipX = true;
		}

		//chek if pressing right button
		if (Input.GetKey(rightKey))
		{
			//Move right, but keep vertical velocity
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<MoveSpeed>().speed,GetComponent<Rigidbody2D>().velocity.y);

			//flip image
			GetComponent<SpriteRenderer>().flipX = false;
		}
	}
}
