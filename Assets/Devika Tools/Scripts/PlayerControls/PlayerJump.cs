﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Player/PlayerJump")] //this addes the compent to the Add Component Menu
public class PlayerJump : MonoBehaviour {

	//the key to make your player jump
	public KeyCode jumpKey = KeyCode.Space;

	//How high to jump
	public float jumpForce = 5f;

	// Update is called once per frame
	void Update ()
	{

		//Jump
		if (Input.GetKeyDown (jumpKey)) 
		{
			//A raycast is like pointing out a lazer from the provided position (transform.position), in the provided direction (-Vector3.up) for the provided distance (0.5f) and seeing it if hits anything.
			RaycastHit2D hit = Physics2D.Raycast (new Vector2 (transform.position.x, transform.position.y), -Vector2.up, 1f, (LayerMask)1);

			// If on the ground and jump is pressed...
			if (hit.collider != null) 
			{
				// ... add force in upwards.
				GetComponent<Rigidbody2D> ().AddForce (Vector2.up * jumpForce, ForceMode2D.Impulse);
			}
		}
	}
}
