﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Player/PlayerInfinateRunner")] //this addes the compent to the Add Component Menu
public class PlayerInfinateRunner : MonoBehaviour {

	//Thi holds the direction and speed you want object to move
	public float Speed = 5f;

	//This holds the acceleration that speeds up the player over time
	public float Acceleration = 0.5f;

	// Update is called once per frame
	void Update () {

		//Add the movement Direction to the curret position. Moving it in that direction
		GetComponent<Rigidbody2D> ().velocity = new Vector2(Speed, GetComponent<Rigidbody2D> ().velocity.y);

		//Increase Speed by acceleration
		Speed += Acceleration * Time.deltaTime; //Time detla time accounts for different frame rates

	}
}
