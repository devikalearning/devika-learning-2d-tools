﻿//by Brennan Hatton - May 2016 brennan@brennanhatton.com

using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Player/PlayerShoot")] //this addes the compent to the Add Component Menu
public class PlayerShoot : MonoBehaviour {

	//A prefab of the bullet
	public Rigidbody2D BulletPrefab;

	//The force the bullet is shot at
	public float ShootForce = 5;

	//The button used to shoot
	public KeyCode ShootButton = KeyCode.E;

	//The position the bullet is shot from
	public Transform BulletSpawnPoint;

	/// The shoot in movement direction.
	public bool shootInMovementDirection = true;


	void Start()
	{
		//If a spawn point was not set
		if (BulletSpawnPoint == null)

			//Use the shooter as the spawn point
			BulletSpawnPoint = transform;
	}

	// Update is called once per frame
	void Update () {

		//Check if shoot is pressed
		if (Input.GetKeyDown (ShootButton)) {

			//Create a bullet and save a reference to it
			Rigidbody2D bullet = Instantiate (BulletPrefab,BulletSpawnPoint.position,BulletSpawnPoint.rotation) as Rigidbody2D;

			//if we want the bullet to move the same direction the shooter is moving
			if (shootInMovementDirection) {

				//Propell the bullet at the current speed of the shoorter, plus its own velocity in the direction the player is moving
				bullet.velocity = GetComponent<Rigidbody2D>().velocity + GetComponent<Rigidbody2D>().velocity.normalized * ShootForce;
			} else 
			//If we want the bullet to move only to the right
			{
				
				//Propell the bullet at the current speed of the shoorter, plus its own velocity
				bullet.velocity = GetComponent<Rigidbody2D> ().velocity + Vector2.right * ShootForce;
			}
		}
	}
}
