﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Player/PlayerMoveCar")] //this addes the compent to the Add Component Menu
public class PlayerMoveCar : MonoBehaviour {


	//Below are variables. Variables are names that hold numbers which can change value.

	//Key controls
	public KeyCode AccelerationForwardKey = KeyCode.W;
	public KeyCode ReverseKey = KeyCode.S;
	public KeyCode leftKey = KeyCode.A;
	public KeyCode rightKey = KeyCode.D;
	public KeyCode brakeKey = KeyCode.Space;

	//acceleration of car
	public float acceleration = 0.1f;

	//how sharp the car turns
	public float turnSpeed = 10f;

	//A reference to the physics componet
	Rigidbody2D rigidbody;

	//A reference to the movement speed;
	MoveSpeed moveSpeed;

	// Use this for initialization
	void Start () {

		//Get the rigidbody component
		rigidbody = this.GetComponent<Rigidbody2D> (); //Calling GetComponent every frame can make the game run slow. 
		//Here we save the result from the GetComponent into a variable, so we dont have to call it in the Update loop

		//Get reference to the MoveSpeed component
		moveSpeed = GetComponent<MoveSpeed> (); 

		//If there is no rigibody componet
		if (rigidbody == null) {

			Debug.LogWarning ("PlayerMovePlatformer component requires Rigidbody2D on " + this.gameObject.name + ". One was added");

		}
	}

	// Update is called once per frame
	void Update () {
		
		//If we are accelerating
		if (Input.GetKey (AccelerationForwardKey)) {
			//if we arent moving
			if (rigidbody.velocity.magnitude < 0.1f)
			{
				//Start moving
				rigidbody.velocity = transform.up * (moveSpeed.speed / 10f);
			}
			//we are already moving
			else 
			{
				//if we are moving foward
				if (Vector2.Angle (rigidbody.velocity.normalized, new Vector2 (transform.up.normalized.x, transform.up.normalized.y)) < 90) 
				{
					//if we havent reached our speed limit
					if (rigidbody.velocity.magnitude < moveSpeed.speed)
					{
						//Accelerate same direction
						rigidbody.velocity += rigidbody.velocity * acceleration;
					}

				} else//we are moving backwards
				{
					//Accelerate opposite direction
					rigidbody.velocity -= rigidbody.velocity * acceleration;
				}
			}

		}

		//fi we are reversing
		if (Input.GetKey (ReverseKey)) {
			//if we arent moving
			if (rigidbody.velocity.magnitude < 0.1f)
				//Start moving
				rigidbody.velocity = -transform.up * (moveSpeed.speed / 10f);
			//we are already moving
			else {
				//if we are moving foward
				if (Vector2.Angle (rigidbody.velocity.normalized, new Vector2 (transform.up.normalized.x, transform.up.normalized.y)) < 90) {

					//Accelerate opposite direction
					rigidbody.velocity -= rigidbody.velocity * acceleration;

				} else {//we are moving backwards
					//if we havent reached our speed limit
					if (rigidbody.velocity.magnitude < moveSpeed.speed /2)
						//Accelerate same direction
						rigidbody.velocity += rigidbody.velocity * acceleration;
				}
			}

		}

		//if we are pressing the brakes
		if (Input.GetKey(brakeKey))
		{
			//stop the car
			if (rigidbody.velocity != Vector2.zero)
				rigidbody.velocity -= rigidbody.velocity / 10;
		}

		//fi we are turning left
		if (Input.GetKey(leftKey))
		{
			//only turn if moving
			if (rigidbody.velocity.magnitude != 0) {
			
				//adjusting turning based on speed and turn speed
				//if the car is going faster, slow down the turning
				float amountToTurn = turnSpeed / (((rigidbody.velocity.magnitude / 5) + moveSpeed.speed * 4 / 5) / moveSpeed.speed);

				//apply turn
				rigidbody.AddTorque(amountToTurn);

			}
		}

		if (Input.GetKey(rightKey))
		{
			//only turn if moving
			if (rigidbody.velocity.magnitude != 0) {

				//adjusting turning based on speed and turn speed
				float amountToTurn = turnSpeed / (((rigidbody.velocity.magnitude / 5) + moveSpeed.speed * 4 / 5) / moveSpeed.speed);

				//apply turn
				rigidbody.AddTorque(-amountToTurn);

			}
		}

		//if we are moving foward
		if (Vector2.Angle (rigidbody.velocity.normalized, new Vector2 (transform.up.normalized.x, transform.up.normalized.y)) < 90) 
		{
			//make sure the car is moving the way it is facing
			rigidbody.velocity = rigidbody.velocity.magnitude * transform.up;
		}
		else //we are moving backwards
		{
			//make sure the car is moving the way it is facing
			rigidbody.velocity = -rigidbody.velocity.magnitude * transform.up;
		}
	}
}
