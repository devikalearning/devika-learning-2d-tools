﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices; //required to control mouse

[AddComponentMenu("Devika/Player/PlayerTurnMouse")] //this addes the compent to the Add Component Menu
public class PlayerTurnMouse : MonoBehaviour {

	//the sensitivity of the mouse control
	public float sensativity = 1;

	//Switches to Invert the mouse axises
	public bool invertedX = false;
	public bool invertedY = false;

	//Does the mouse get repositioned to the center of the screen>
	public bool repositionMouse = true;

	//Storing the value fo the last position the mouse was to calculare how far it has moved
	Vector2 lastMousePosition;

	void Start()
	{
		//Update last mouse position to avoid errors when starting
		lastMousePosition =  Input.mousePosition;

		//Display warning.
		Debug.LogWarning ("!Important! This script may contrains bugs that take over the mouse. Hold Backspace to regain control over mouse.");
	}
	
	// Update is called once per frame
	void Update () {

		//Yaw
		//calculate angle of yaw rotation based on mouse x position 
		float angle = (lastMousePosition.x - Input.mousePosition.x) * Time.deltaTime * sensativity;
		//apply rotation
		this.transform.RotateAround (transform.position, Vector3.forward, angle);

		//Pitch
		//calculate angle of pitch rotation based on mouse y position
		angle = (lastMousePosition.y - Input.mousePosition.y) * Time.deltaTime * sensativity;
		//apply rotation
		this.transform.RotateAround (transform.position, transform.right, angle);

		//update last mouse position
		lastMousePosition = Input.mousePosition;

		//if we are repositioning the mouse and we are not trying ot regain control over the mouse
		if(repositionMouse && !Input.GetKey(KeyCode.Backspace))
		{
			//Change the mouse postioin to center of the camera
			SetCursorPos (Camera.main.pixelWidth/2,Camera.main.pixelHeight/2);//(800, 60);// The values 800, and 60 put the mouse directly ontop of the play button in th editor
		}
	}


	//This imports a function from a Dynamic Linking Library (dll) called user32. This is a system file, beloning to the operating system
	//Without this, we cant use SetCursorPos in the update loop
	[DllImport("user32.dll")]
	public static extern bool SetCursorPos (int X, int Y);
}
