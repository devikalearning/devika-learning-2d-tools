﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Player/PlayerShootClick")] //this addes the compent to the Add Component Menu
public class PlayerShootClick : MonoBehaviour {

	//A prefab of the bullet
	public Rigidbody2D BulletPrefab;

	//The force the bullet is shot at
	public float ShootForce = 5;

	//The position the bullet is shot from
	public Transform BulletSpawnPoint;

	void Start()
	{
		//If a spawn point was not set
		if (BulletSpawnPoint == null)

			//Use the shooter as the spawn point
			BulletSpawnPoint = this.transform;
	}
		
	void Update () {

		//Check if left mouse button is pressed
		if (Input.GetMouseButtonDown (0) )
		{

			//Create a bullet and save a reference to it
			Rigidbody2D bullet = Instantiate (BulletPrefab,BulletSpawnPoint.position,BulletSpawnPoint.rotation) as Rigidbody2D;

			//calulate forces to propell bullet towards mouse
			float xForce = Camera.main.ScreenToWorldPoint (Input.mousePosition).x - transform.position.x;
			float yForce = Camera.main.ScreenToWorldPoint (Input.mousePosition).y - transform.position.y;


			//Propell the bullet at the current speed of the shoorter, plus its own velocity
			bullet.velocity = new Vector2(xForce, yForce).normalized * ShootForce + GetComponent<Rigidbody2D>().velocity;

		}
	}
}
