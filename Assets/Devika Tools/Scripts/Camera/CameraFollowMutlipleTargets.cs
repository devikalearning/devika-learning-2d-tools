﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Camera/CameraFollowMutlipleTargets")] //this addes the compent to the Add Component Menu
public class CameraFollowMutlipleTargets : MonoBehaviour {

	//List of target objects to follow
	Target[] Targets;

	//How far the camer will zoom out
	public float zoomMax = 10;

	//How far the camer will zoom in
	public float zoomMin = 5;
	public bool lockX = false;
	public bool lockY = true;
	public bool lockZ = false;

	//The start position of the camera for calculating how high or low the camera will stay
	Transform startTransform;

	//Reference to camera
	Camera camera;

	float LeftMost, RightMost;
	Vector3 distance;

	//The x position the camera will sit
	float Targetx;
	float Targety;
	float Targetz;



	// Use this for initialization
	void Start () {

		//Update list of targets
		Targets = this.GetComponents<Target>();

		// Save starting position
		startTransform = this.transform;

		//Get reference to camera
		camera = this.GetComponent<Camera>();
	}

	// Update is called once per frame
	void Update () {

		//Update list of targets
		Targets = this.GetComponents<Target>();

		distance = Vector3.zero;


		//Reset variables for recalculation 
		Targetx = 0;

		LeftMost = Targets [0].target.position.x;
		RightMost = Targets [0].target.position.x;

		//Go through all targets
		//For each target, do the follow code.
		for (int i = 0; i < Targets.Length; i++) {

			//Check if this target is left most
			if (Targets [i].target.position.x < LeftMost)
				LeftMost = Targets [i].target.position.x;

			//Check if this target is right most
			if (Targets [i].target.position.x > RightMost)
				RightMost = Targets [i].target.position.x;

			//add the x position of this target
			Targetx += Targets [i].target.position.x;
		}

		//Calculate average
		//Devide the total of all x positions of targets by the number of targets
		Targetx = Targetx / Targets.Length;


		//Calulate distance between right most and left most targets
		distance = new Vector3( Mathf.Abs(LeftMost - RightMost), distance.y,distance.z);
		



		//Reset variables for recalculation
		Targety = 0;

		LeftMost = Targets [0].target.position.y;
		RightMost = Targets [0].target.position.y;

		//Go through all targets
		//For each target, do the follow code.
		for (int i = 0; i < Targets.Length; i++) {

			//Check if this target is left most
			if (Targets [i].target.position.y < LeftMost)
				LeftMost = Targets [i].target.position.y;

			//Check if this target is right most
			if (Targets [i].target.position.y > RightMost)
				RightMost = Targets [i].target.position.y;

			//add the x position of this target
			Targety += Targets [i].target.position.y;
		}

		//Calculate average
		//Devide the total of all x positions of targets by the number of targets
		Targety = Targety / Targets.Length;

		//Calulate distance between right most and left most targets
		distance = new Vector3( distance.x, Mathf.Abs(LeftMost - RightMost), distance.z);
	



		//Reset variables for recalculation
		Targetz = 0;

		LeftMost = Targets [0].target.position.z;
		RightMost = Targets [0].target.position.z;

		//Go through all targets
		//For each target, do the follow code.
		for (int i = 0; i < Targets.Length; i++) {

			//Check if this target is left most
			if (Targets [i].target.position.z < LeftMost)
				LeftMost = Targets [i].target.position.z;

			//Check if this target is right most
			if (Targets [i].target.position.z > RightMost)
				RightMost = Targets [i].target.position.z;

			//add the x position of this target
			Targetz += Targets [i].target.position.z;
		}

		//Calculate average
		//Devide the total of all x positions of targets by the number of targets
		Targetz = Targetz / Targets.Length;

		//Calulate distance between right most and left most targets
		distance = new Vector3(distance.x, distance.y, Mathf.Abs(LeftMost - RightMost));



		//Move the camera to have the same x positoin as the newly calculated target x
		this.transform.position = new Vector3 (
			lockX ? startTransform.position.x : Targetx, 
			lockY ? startTransform.position.y : Targety, 
			lockZ ? startTransform.position.z : Targetz
		);
		

		//look at centre
		transform.LookAt(new Vector3 (Targetx, Targety,Targetz));

		//Change the view size of the camera
		camera.orthographicSize = Mathf.Min (Mathf.Max (distance.magnitude / 3f, zoomMin), zoomMax);

	}
}
