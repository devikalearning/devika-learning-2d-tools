﻿//by Brennan Hatton - May 2016 brennan@brennanhatton.com

using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Camera/PlatformerCameraMutlipleTargets")] //this addes the compent to the Add Component Menu
public class PlatformerCameraMutlipleTargets : MonoBehaviour {

	//List of target objects to follow
	public Transform[] Targets;

	//How far the camer will zoom out
	public float zoomMax = 10;
		
	//How far the camer will zoom in
	public float zoomMin = 5;

	//The start position of the camera for calculating how high or low the camera will stay
	Transform startTransform;

	//Reference to camera
	Camera camera;

	float LeftMost, RightMost;
	float distance;
	//The x position the camera will sit
	float Targetx;


	// Use this for initialization
	void Start () {

		// Save starting position
		startTransform = this.transform;

		//Get reference to camera
		camera = this.GetComponent<Camera>();
	}

	// Update is called once per frame
	void Update () {

		//Reset variables for recalculation
		Targetx = 0;
		LeftMost = Targets [0].position.x;
		RightMost = Targets [0].position.x;

		//Go through all targets
		//For each target, do the follow code.
		for (int i = 0; i < Targets.Length; i++) {

			//Check if this target is left most
			if (Targets[i].position.x < LeftMost)
				LeftMost = Targets[i].position.x;

			//Check if this target is right most
			if (Targets[i].position.x > RightMost)
				RightMost = Targets[i].position.x;

			//add the x position of this target
			Targetx += Targets[i].position.x;
		}
			
		//Calculate average
		//Devide the total of all x positions of targets by the number of targets
		Targetx = Targetx / Targets.Length;

		//Move the camera to have the same x positoin as the newly calculated target x
		this.transform.position = new Vector3 (Targetx, startTransform.position.y, startTransform.position.z);

		//Calulate distance between right most and left most targets
		distance = Mathf.Abs(LeftMost - RightMost);

		//Change the view size of the camera
		camera.orthographicSize = Mathf.Min (Mathf.Max (distance / 3f, zoomMin), zoomMax);

	}
}
