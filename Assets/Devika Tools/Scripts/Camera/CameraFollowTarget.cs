﻿//by Brennan Hatton - May 2016 brennan@brennanhatton.com

using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Camera/CameraFollowTarget")] //this addes the compent to the Add Component Menu
public class CameraFollowTarget : MonoBehaviour {

	//The initail transform for the camera
	Transform startTransform;

	//Switches to either follow the transform of the target or stick to original transform
	public bool xPosition = true;
	public bool yPosition = true;
	public bool zPosition = true;
	public bool	xRotation = true;
	public bool yRotation = true;
	public bool zRotation = true;

	// Use this for initialization
	void Start () {

		//save the starting transform
		startTransform = this.transform;
	}
	
	// Update is called once per frame
	void Update () {

		//Set the transform
		//The syntax used here allows for "if" statement to be put inside of a function call.
		//The syntax is as follows
		// (if this is true) ? this result will run if it is true : this result will run if testBool is false
		this.transform.position = new Vector3 (
			(xPosition ? GetComponent<Target>().target.transform.position.x : startTransform.position.x), 
			(yPosition ? GetComponent<Target>().target.transform.position.y : startTransform.position.y), 
			(zPosition ? GetComponent<Target>().target.transform.position.z : startTransform.position.z)
		);

		//Set the rotation
		this.transform.rotation = Quaternion.Euler(
			(xRotation ? GetComponent<Target>().target.transform.rotation.eulerAngles.x : this.transform.rotation.eulerAngles.x), 
			(yRotation ? GetComponent<Target>().target.transform.rotation.eulerAngles.y : this.transform.rotation.eulerAngles.y), 
			(zRotation ? GetComponent<Target>().target.transform.rotation.eulerAngles.z : this.transform.rotation.eulerAngles.z)
		);
	}
}
