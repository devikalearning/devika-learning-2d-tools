﻿//by Brennan Hatton - May 2016 brennan@brennanhatton.com

using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Camera/PlatformerCameraFollowTarget")] //this addes the compent to the Add Component Menu
public class PlatformerCameraFollowTarget : MonoBehaviour {

	//The camera target
	public Transform Target;

	//The position the camera starts
	Transform startTransform;


	// Use this for initialization
	void Start () {

		//Save the position where the camera starts
		startTransform = this.transform;
	}

	// Update is called once per frame
	void Update () {

		//We only want to move the camera along the x. Becuase this is a side scrolling platformer.
		//Update only the x position so we can move the camera along the x axis
		this.transform.position = new Vector3 (
			Target.position.x, 
			startTransform.position.y,  //By putting the current y position as the new y position, it shouldnt change
			startTransform.position.z //we do the same for the z as we did for the y above
		);

	}
}
