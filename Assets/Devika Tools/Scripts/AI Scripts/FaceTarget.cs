﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/AI/FaceTarget")] //this addes the compent to the Add Component Menu
public class FaceTarget : MonoBehaviour {

	// a place to store the initial rotation
	Vector3 startRotation;

	// Use this for initialization
	void Start () 
	{
		//save initial rotation
		startRotation = this.transform.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Get the vector 3 direction between target and self position.
		//(this doesnt need to be a vector 3, but beuase the position of transforms are already Vector 3, we cna do the same math and just not use the z component.
		Vector3 diff = GetComponent<Target>().target.position - transform.position;

		//normalize vector
		diff.Normalize();

		//get the z rotation based on the angle of the x and y (and turn it into radians
		float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;

		//apple rotation around z axis
		transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

		//apply initial rotation
		transform.Rotate (startRotation);

	}
}
