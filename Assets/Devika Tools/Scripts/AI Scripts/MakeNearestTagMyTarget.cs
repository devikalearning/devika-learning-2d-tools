﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/AI/MakeNearestTagMyTarget")] //this addes the compent to the Add Component Menu
public class MakeNearestTagMyTarget : MonoBehaviour {

	//the tag we are looking for
	public string tagetTag;

	//Dont target myself
	public bool DontTargetMyself = true;

	//if we only want to perform the serach on start
	public bool OnlyWhenLostTarget = true;

	//if we only want to perform the serach on start
	public bool OnlyOnce = false;

	//a palce to save a refernec to the Target Component once we have got it.
	Target targetComponent;

	//a timer indicating if we have already earched recently.
	float readyToSearch;

	void Start()
	{
			
		//Get the target comonent, so we dont have to keep getting it every frame
		targetComponent = GetComponent<Target> ();

		//We havent saerched yet! Make it zero
		readyToSearch = 0;

		//See if a target already exists
		LookForTaget ();
	}

	// Update is called once per frame
	void Update () {
	
		//if we are only search once, we did it in the start funciton. No need in the update loop
		if (OnlyOnce) 
		{
			//exit the update loop and skip the rest
			return;
		}
		
		//if we dot have a target or we dont care
		if (targetComponent.target == null || OnlyWhenLostTarget == false)
		{
			//if we havent already searched recently or we have lost target
			if (readyToSearch <= 0 || OnlyWhenLostTarget)
			{
				LookForTaget ();

			}else
				//Dont search for another 2 seconds
				readyToSearch = 2;
		}else
			//Count down seconds until can search
			readyToSearch -= Time.deltaTime;
}
	/// <summary>
	/// Looks for taget.
	/// </summary>
	void LookForTaget()
	{
		
		//Get all tag objects
		GameObject[] tagObjects = GameObject.FindGameObjectsWithTag(tagetTag);

				//if we found some
		if (tagObjects.Length > 0) 
		{
			//Save first one
			GameObject closestObj = tagObjects [0];


			//Check if me
			if (DontTargetMyself) {

				//if found myself
				if (closestObj == this.gameObject) {


					//if no more objects
					if (tagObjects.Length <= 1) {

						//exit
						return;
					}

					//start on next object
					closestObj = tagObjects [1];

				}
			}


			//Calculate distance to first one
			float nextDistance, closestDistance = Vector3.Distance (this.transform.position, tagObjects [0].transform.position);
					
			//For all other objects
			for (int i = 1; i < tagObjects.Length; i++) {

				//Check if me
				if (DontTargetMyself) {

					//if found myself
					if (tagObjects [i].gameObject == this.gameObject) {
						
						//Move onto next object
						i++;

						//if no more objects
						if (i >= tagObjects.Length) {
							
							//Update target
							targetComponent.target = closestObj.transform;

							//exit
							return;
						}
							
					};

				}
				//calculate distance to object
				nextDistance = Vector3.Distance (this.transform.position, tagObjects [i].transform.position);

				//if this object is closer
				if (nextDistance < closestDistance) {
					//update closest distance
					closestDistance = nextDistance;

					//save obejct as closest so far
					closestObj = tagObjects [i];
				}

			}

			//Update target
			targetComponent.target = closestObj.transform;
		}
				
	}
}
