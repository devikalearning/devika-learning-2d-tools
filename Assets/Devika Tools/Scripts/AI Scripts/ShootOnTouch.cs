﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/AI/ShootOnTouch")] //this addes the compent to the Add Component Menu
public class ShootOnTouch : MonoBehaviour {
	
	//Reference to the bullet prefab that will be shot.
	public Rigidbody2D BulletPrefab;

	//The force that will be applied to the bullet when shot
	public Vector3 ShootForce;

	//The position the bullet will be shot from
	public Transform BulletSpawnPoint;

	//How long between shots
	public float reloadTime;

	//Do we use the ammo component or is it unlimited?
	public bool useAmmo = false;

	public bool ignoreTriggers = false;

	//Time counting down how long is left until finished reloading
	float reloadingTimer = 0;

	void Start()
	{
		//If a spawn point was not set
		if (BulletSpawnPoint == null)
		{
			//It is best to set a spawn point, but if not spawn point was set than it will use itself
			Debug.LogWarning("Transform BulletSpawnPoint was no set for " + this.gameObject.name + ". It will use itself as a spawn point which could create collision errors. If this was intended, drag a reference of itself into BulletSpawnPoint");

			//Use the shooter as the spawn point
			BulletSpawnPoint = this.transform;
		}
	}

	void Update()
	{
		if (reloadingTimer > 0) 
		{
			//update reloading timer
			reloadingTimer -= Time.deltaTime;
		}

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		//if we are ignoring triggers
		if (ignoreTriggers)

			//exit here
			return;

		if (reloadingTimer <= 0) {

			//if we are using ammo
			if (useAmmo) {

				//if we have ammo left
				if (GetComponent<Ammo>(). AmmoLeft())
				{
					//use one ammo
					GetComponent<Ammo>().ammo--;
				}else //if thre is no ammo left
				{
					//Stop here
					return;
				}
			}

			//Create a bullet and save a reference to it
			Rigidbody2D bullet = Instantiate (BulletPrefab, BulletSpawnPoint.position, BulletSpawnPoint.rotation) as Rigidbody2D;

			//Set the bullet speed to move forwards
			bullet.velocity = ShootForce;

			//Start the reload timer
			reloadingTimer = reloadTime; 


		} 
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (reloadingTimer <= 0) {

			//if we are using ammo
			if (useAmmo) {

				//if we have ammo left
				if (GetComponent<Ammo>(). AmmoLeft())
				{
					//use one ammo
					GetComponent<Ammo>().ammo--;
				}else //if thre is no ammo left
				{
					//Stop here
					return;
				}
			}

			//Create a bullet and save a reference to it
			Rigidbody2D bullet = Instantiate (BulletPrefab, BulletSpawnPoint.position, BulletSpawnPoint.rotation) as Rigidbody2D;

			//Set the bullet speed to move forwards
			bullet.velocity = ShootForce;

			//Start the reload timer
			reloadingTimer = reloadTime; 


		} 
	}
}
