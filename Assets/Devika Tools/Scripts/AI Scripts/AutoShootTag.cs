﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/AI/AutoShootTag")] //this addes the compent to the Add Component Menu
public class AutoShootTag : MonoBehaviour {

	//Reference to the bullet prefab that will be shot.
	public Rigidbody2D BulletPrefab;

	//The force that will be applied to the bullet when shot
	public float ShootForce;

	//The position the bullet will be shot from
	public Transform BulletSpawnPoint;

	//How long between shots
	public float reloadTime;

	//The tag all enemies that are being targetted
	public string EnemyTag;

	//The distance an enemy will be shot at
	public float AttackingRange;

	//Time counting down how long is left until finished reloading
	float reloadingTimer = 0;

	//The position of the enemy being targetted
	Transform target;

	//Do we use the ammo component or is it unlimited?
	public bool useAmmo = false;

	void Start()
	{
		//If a spawn point was not set
		if (BulletSpawnPoint == null)
		{
			//It is best to set a spawn point, but if not spawn point was set than it will use itself
			Debug.LogWarning("Transform BulletSpawnPoint was no set for " + this.gameObject.name + ". It will use itself as a spawn point which could create collision errors. If this was intended, drag a reference of itself into BulletSpawnPoint");

			//Use the shooter as the spawn point
			BulletSpawnPoint = this.transform;
		}
	}

	// Update is called once per frame
	void Update () {

		if (reloadingTimer <= 0) {

			//If we dont have an enemy targetted
			if (target == null) {
				//Find an enemy
				FindEnemy ();
			}

			//We do have an eneemy targetted
			//Is the enemy close enough to shoot?
			else if (Vector3.Distance (target.position, this.transform.position) < AttackingRange) {

				//if we are using ammo
				if (useAmmo) {

					//if we have ammo left
					if (GetComponent<Ammo> ().AmmoLeft ()) {
						//use one ammo
						GetComponent<Ammo> ().ammo--;
					} else { //if thre is no ammo left
						//Stop here
						return;
					}
				}

				//Create a bullet and save a reference to it
				Rigidbody2D bullet = Instantiate (BulletPrefab, BulletSpawnPoint.position, BulletSpawnPoint.rotation) as Rigidbody2D;

				//Have bullet face target so forwards is towards target
				bullet.transform.LookAt (target.position);

				//Set the bullet speed to move forwards
				bullet.velocity = bullet.transform.forward * ShootForce;

				//Start the reload timer
				reloadingTimer = reloadTime; 

			} else
				target = null;

		} else { //Still reloading
			//update reloading timer
			reloadingTimer -= Time.deltaTime;
		}
	}

	//<summary>
	//Find the first enemy close enough to attack
	//</summary>
	void FindEnemy()
	{
		//Get a list of all enemies
		GameObject[] Enemies = GameObject.FindGameObjectsWithTag(EnemyTag);

		//for each enemy in the enemy list
		for (int i = 0; i < Enemies.Length; i++) 
		{
			//Check if it is close enough to attack
			if (Vector3.Distance (Enemies [i].transform.position, this.transform.position) <= AttackingRange) 
			{
				//We have found our target!
				target = Enemies [i].transform;
				return;
			}
		}

		return;
	}
}
