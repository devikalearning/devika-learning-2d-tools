﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/AI/SameMoveSpeedAsTarget")] //this addes the compent to the Add Component Menu
public class SameMoveSpeedAsTarget : MonoBehaviour {

	//How much faster than the target (or negative for slower)
	public float faster = 0;

	//Will only perform on start
	public bool OnlyOnce;

	void Start()
	{
		GetComponent<MoveSpeed> ().speed = GetComponent<Target> ().GetComponent<MoveSpeed> ().speed + faster;

	}

	// Update is called once per frame
	void Update () {
	
		//if this is not only done once, it is done all the time!
		if (OnlyOnce = false) 
		{
			//Update speed
			GetComponent<MoveSpeed> ().speed = GetComponent<Target> ().GetComponent<MoveSpeed> ().speed + faster;
		}
	}
}
