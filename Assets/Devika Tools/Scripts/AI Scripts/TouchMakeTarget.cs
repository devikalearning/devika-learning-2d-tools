﻿using UnityEngine;
using System.Collections;

public class TouchMakeTarget : MonoBehaviour {

	//Tag of object needed to touch
	[Tooltip("Leave blank for no specific tag, will work with all objects")]//Tool tips show up in the inspector
	public string tag;

	//Will not create from touching triggers
	public bool notTriggers = false;

	//When this object hits a trigger
	void OnTriggerEnter2D(Collider2D other)
	{
		//if we are not creating on triggers
		if (notTriggers) {
			//exit this function, we dont want to contiune to creating the game object
			return;
		}

		//if tag is empty or correct
		if (tag == null || tag == "" || other.tag == tag) 
		{
			//Make target
			GetComponent<Target>().target = other.gameObject.transform;
		}
	}

	//When ths object has a collision
	void OnCollisionEnter2D(Collision2D col)
	{

		//if tag is empty or correct
		if (tag == null || tag == ""  || col.gameObject.tag == tag)
		{
			//Make target
			GetComponent<Target>().target = col.gameObject.transform;
		}
	}
}
