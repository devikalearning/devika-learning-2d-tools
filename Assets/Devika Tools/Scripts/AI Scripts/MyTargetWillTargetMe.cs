﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/AI/MyTargetWillTargetMe")] //this addes the compent to the Add Component Menu
public class MyTargetWillTargetMe : MonoBehaviour {

	//do we want to replace an excisting target?
	public bool ReplaceTaret = true;

	//do we want to make sure the other object is always targeting this object?
	public bool Always = false;

	Target otherTargetComponent;

	// Use this for initialization
	void Start () {

		//if we are replacing the current target
		if (ReplaceTaret) {
			
			//Get the existing target component
			otherTargetComponent = GetComponent<Target>().target.gameObject.GetComponent<Target> ();
			//Make it target me
			otherTargetComponent.target = transform;

		} else {

			//Add another target
			otherTargetComponent = GetComponent<Target>().target.gameObject.AddComponent<Target> () as Target;
			//Make it target me
			otherTargetComponent.target = transform;
		}

	}
	
	// Update is called once per frame
	void Update () {
	
		//Do we want to keep making sure it is targetting us?
		if (Always)
		{
			//Update others target
			otherTargetComponent.target = transform;
		}

	}
}
