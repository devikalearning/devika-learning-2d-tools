﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/Animate/AnimateMove")] //this addes the compent to the Add Component Menu
public class AnimateMove : MonoBehaviour {

	//a reference to The animation controller to send messages to about the animation
	Animator animator;

	//a reference to the rigidbody component
	Rigidbody2D rigidBody;

	//a reference to the sprint component
	PlayerSprint sprint;

	// Use this for initialization
	void Start () {
	
		//get the animator and save a reference to it.
		animator = GetComponent<Animator> ();

		//get the rigidbody and save a reference ot it
		rigidBody = GetComponent<Rigidbody2D> ();

		//get the playersprint and save a reference ot it
		sprint = GetComponent<PlayerSprint> ();

	}
	
	// Update is called once per frame
	void Update () {

		//if this object has a playersprint
		if (sprint != null) 
		{
			//if it is sprinting
			if (rigidBody.velocity.magnitude >= sprint.sprintSpeed) 
			{
				//TUrn on sprint animation
				animator.SetFloat ("Running", rigidBody.velocity.magnitude);

				//turn off walk animaiton
				animator.SetFloat ("Walking", 0);

			} else //it is walking
			{
				//Turn on walking animaiton
				animator.SetFloat ("Walking", rigidBody.velocity.magnitude);

				//turn off walking animation
				animator.SetFloat ("Running", 0);
			}
				

		} else //if this object doesnt have a playersprint
		{
			//turn on walk animation
			animator.SetFloat ("Walking", rigidBody.velocity.magnitude);

			//turn off run animation
			animator.SetFloat ("Running", 0);
		}

	}
}
