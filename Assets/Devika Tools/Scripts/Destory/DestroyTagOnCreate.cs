﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnDestory/DestroyTagOnCreate")] //this addes the compent to the Add Component Menu
public class DestroyTagOnCreate : MonoBehaviour {

	//tag to look for and destory
	public string TagToDestory;

	//dont destory self if found
	public bool notSelf = true;

	// Use this for initialization
	void Start () {
	
		//get a list of all objects with that tag 
		GameObject[] objectsToDestroy = GameObject.FindGameObjectsWithTag (TagToDestory);

		//loop through each objct we found
		for (int i = 0; i < objectsToDestroy.Length; i++) {

			//if it is not ourself
			if (objectsToDestroy [i] != this.gameObject) {

				//destory it
				DestroyObject (objectsToDestroy [i]);


			} else //if it is ourself and we are looking for ourself
				if (notSelf == false) {
				//destory ourselfs after 1 second (so we can destroy all the other objects first)
				DestroyObject (this.gameObject, 1f);
			}
		}

	}

}
