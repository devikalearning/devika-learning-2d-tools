﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnDestory/ScoreOnDestroy")] //this addes the compent to the Add Component Menu
public class ScoreOnDestroy : MonoBehaviour {

	public int score;

	void OnDestroy () {

		//Add the score
		GameController.instance.score += score;
	}
}
