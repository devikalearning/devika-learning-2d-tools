﻿using UnityEngine;
using System.Collections;

public class CreateOnDeath : MonoBehaviour {

	//Object ot be created when destoryed
	public GameObject gameObjectToCreate;

	//the position and rotation this object will be created at
	[Tooltip("Leave blank to use own transform")]
	public Transform targetTransform;

	void Start()
	{
		Debug.LogWarning ("This function is soon to be obselete. It is not advvies to be used as it can create serious editors that cna persist after you game stops running. Perhaps create you object by the same means you use ot destory this one. ");

		//if no transform is set
		if (targetTransform == null) 
		{
			//use own transform
			targetTransform = this.transform;
		}
	}

	//When object is destoryed
	void OnDestroy()
	{
		//create object
		Instantiate (gameObjectToCreate, targetTransform.position , targetTransform.rotation);

		Debug.Log ("OnDestroy called from CreateOnDeath - Exit now to avoid error");

	}
}
