﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnDestory/DestroyOnDistanceFromTarget")] //this addes the compent to the Add Component Menu
public class DestroyOnDistanceFromTarget : MonoBehaviour {

	//distance require to destory
	public float maxDistance;

	//How close can the object get to the target before it is destroyed
	public float minDistance;

	// Update is called once per frame
	void Update () {
	
		//If the distance between this object and its target are greater than the distance require to destroy
		if (Vector3.Distance (transform.position, GetComponent<Target> ().target.transform.position) > maxDistance) 
		{
			//if the distance is less than the minimum distance
			if (Vector3.Distance (transform.position, GetComponent<Target> ().target.transform.position) < minDistance) {

				//Destory this object
				DestroyObject (this.gameObject);
			}
		}

	}
}
