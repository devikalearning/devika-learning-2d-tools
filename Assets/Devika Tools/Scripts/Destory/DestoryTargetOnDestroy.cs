﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnDestory/DestoryTargetOnDestroy")] //this addes the compent to the Add Component Menu
public class DestoryTargetOnDestroy : MonoBehaviour {


	//Object ot be created when destoryed
	public GameObject gameObjectToCreate;

	//When object is destoryed
	void OnDestroy()
	{
		//create object
		DestroyObject (gameObjectToCreate);
	}
}
