﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/OnDestory/DestroyOnAmmo")] //this addes the compent to the Add Component Menu
public class DestroyOnAmmo : MonoBehaviour {

	//destorys this object when ammo is value of this variable
	public int DestroyWhenAmmoIs = 0;

	// Update is called once per frame
	void Update () {
	
		//if ammo is target
		if (GetComponent<Ammo>().ammo == DestroyWhenAmmoIs) {

			//destroy this obect
			DestroyObject (this.gameObject);
		}
	}
}
