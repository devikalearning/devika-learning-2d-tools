﻿using UnityEngine;
using System.Collections;

public class TetrisGameController : MonoBehaviour {

	public DuplicateObject arrayCreator;

	//reference to array of objets
	Target[,] blocks;

	Vector2 arraySize;

	// Use this for initialization
	void Start () {
	
		//opoulate array of blocks
		GameObject[] blocksList = GameObject.FindGameObjectsWithTag("CubeSpace");

		//get array size`
		arraySize = arrayCreator.numberOfObjects;

		//create array of size in x & yaxis
		blocks = new Target[(int)arraySize.x,(int)arraySize.y];

		//populate 2d array based on position
		for (int i = 0; i < blocksList.Length; i++) {
			int x = (int)(blocksList [i].transform.position.x - arrayCreator.targetTransform.position.x);
			int y = (int)(blocksList [i].transform.position.y - arrayCreator.targetTransform.position.y);

			//use x and y for position in array
			blocks [x,y] = blocksList [i].GetComponent<Target>();
		}


	}
	
	// Update is called once per frame
	void Update () {
	
		//check if row is won
		for(int y = 0; y < (int)arraySize.y; y++)
		{
			bool won = true;

			for(int x = 0; x < (int)arraySize.x; x++)
			{
				if (blocks [x, y].target == null) {
					won = false;
				} else if (Vector3.Distance (blocks [x, y].target.position, blocks [x, y].transform.position) >= arrayCreator.offset.y)
					won = false;
				//else
				//	blocks [x, y].GetComponent<Renderer> ().material.color = Color.red;
			}

			if (won)
			{
				for(int x = 0; x < (int)arraySize.x; x++)
				{
					DestroyObject (blocks [x, y].target);
				}
			}
		}
			

	}
}
