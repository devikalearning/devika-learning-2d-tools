﻿using UnityEngine;
using System.Collections;

public class LockToGridTransform : MonoBehaviour {

	//
	public Vector3 gridSize = Vector3.one; 

	public bool OnlyOnStart = true;

	// Use this for initialization
	void Start () {

		transform.position = new Vector3 ((float)(int)(transform.position.x / gridSize.x), (float)(int)(transform.position.y / gridSize.y), (float)(int)(transform.position.z / gridSize.z));
	}
	
	// Update is called once per frame
	void Update () {
	
		if(!OnlyOnStart)
			transform.position = new Vector3 ((float)(int)(transform.position.x / gridSize.x), (float)(int)(transform.position.y / gridSize.y), (float)(int)(transform.position.z / gridSize.z));
	}
}
