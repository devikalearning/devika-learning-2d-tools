﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Devika/GameControls/MultiSpawnObject")] //this addes the compent to the Add Component Menu
public class MultiObjectSpawner : MonoBehaviour {

	//This si the objct we will be spawning
	public GameObject[] objectsToSpawn;

	//How often it is spawned
	public float frequency = 10f;

	//Distance from position they are to be spawned
	public float minDistance = 10f;
	public float maxDistance = 15f;

	//position objects are spawned from
	public Transform position;

	//do we select in order, or one at a time
	public bool SelectRandomly = true;

	public bool spawnOnXAxis = false;
	public bool spawnOnYAxis = false;


	//how long until next spawn
	float timer;

	// Use this for initialization
	void Start () {

		//reset timer
		timer = frequency;

		//if not posotion is provided use self
		if (position == null) {
			position = this.transform;
		}

	}

	// Update is called once per frame
	void Update () {

		//if its time for next object
		if (timer <= 0) {

			//Calculate position on circle
			float angle = Random.value * Mathf.PI * 2;
			//float radius = _random.NextDouble() * _radius;
			float distance = minDistance + (maxDistance - minDistance) * Random.value;
			float x, y;

			//if we are spawning on x axis
			if (spawnOnXAxis) {
				//use x axis
				x = position.position.x;
			} else //we are not spawning on x axis
			{
				//spawn on circle
				x = position.position.x + minDistance * Mathf.Cos (angle);
			}

			//if we are spawning on y axis
			if (spawnOnYAxis) {
				//use y axis
				y = position.position.y;
			} else {
				//spawn on circle
				y = position.position.y + minDistance * Mathf.Sin(angle);
			}

			//pick an object
			int id = Random.Range(0,objectsToSpawn.Length);
			//Create object
			Instantiate (objectsToSpawn[id], new Vector3(x,y,0f), objectsToSpawn[id].transform.rotation);

			//reset timer
			timer = frequency;

		}else
		{
			//update timer
			timer -= Time.deltaTime;
		}
	}
}
