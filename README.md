# README #

This project holds shared code for various games made at the powerhouse museum.

### What is this repository for? ###

* Unity5

### How do I get set up? ###

* To use this project in class. 
Select the scene of the game you wish to teach. 
Also select the MainMenu Scene.
Right click and select -> Export Package
Make sure "Include Dependencies" is selected
Now hit Export, and save it as the name of the class you are teaching.

Now create a new project. This will be the one the students will edit.
Name appropriately.
Import the package you just created above. 
Copy over any other scripts, prefabs or other assets you might want the students to use.

If you are teaching a coding class
- Delete the scripts.


### Who do I talk to? ###

* James, Mark, Curtis & Brennan